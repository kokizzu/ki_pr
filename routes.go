package main

import (
	"bitbucket.org/kokizzu/ki_pr/controller"
	"bitbucket.org/kokizzu/ki_pr/controller/cGuest"
	"bitbucket.org/kokizzu/ki_pr/controller/cMy"
	"bitbucket.org/kokizzu/ki_pr/controller/cSuperadmin"
	"bitbucket.org/kokizzu/ki_pr/go/rest"
)

var ROUTES = map[string]rest.ReqHandler{
	``: controller.Index,

	`guest/forgot_password`:           cGuest.ForgotPassword,
	`guest/google_login`:              cGuest.GoogleLogin, // google oauth callback
	`guest/login`:                     cGuest.Login,
	`guest/register`:                  cGuest.Register,
	`guest/reset_password/:code/:uid`: cGuest.ResetPassword,
	`guest/verify_email/:code/:uid`:   cGuest.VerifyEmail,

	`my/logout`:  cMy.Logout,
	`my/profile`: cMy.Profile,

	`superadmin/users`: cSuperadmin.Users,
}

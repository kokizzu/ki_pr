package rest

import (
	"bytes"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/S"
	"github.com/valyala/fasthttp"
	"time"
)

type ReqHandler func(*HttpCtx)

type HttpCtx struct {
	*fasthttp.RequestCtx
	IsApiCall    bool // all post considered REST API call
	Posts        M.SS
	ApiAction    string // API call method name
	Actor        int64  // requester, 0 = guest
	JsonResponse M.SX
	IsDebug      bool
	*WebServer
	MenuBuffer   bytes.Buffer
	Session      M.SX
	FirstSegment string
	Now          time.Time
}

const ApiActionKey = `a` // the post form hidden value

// create a new request and response context
func NewHttpCtx(ws *WebServer, ctx *fasthttp.RequestCtx) *HttpCtx {
	isApiCall := !ctx.IsGet()
	newCtx := &HttpCtx{
		RequestCtx:   ctx,
		WebServer:    ws,
		IsApiCall:    isApiCall,
		JsonResponse: M.SX{},
		Now:          time.Now(),
	}
	// check first segment
	segments := S.Split(ctx.Request.URI().String(), `/`)
	const segmentIdx = 3 // http:0/1/hostname2/segment3
	if len(segments) > segmentIdx {
		newCtx.FirstSegment = segments[segmentIdx]
	}
	// load session
	newCtx.LoadSession()
	// Web or API specific
	if isApiCall { // parse POST
		posts := ctx.PostArgs()
		newCtx.Posts = M.SS{}
		posts.VisitAll(func(key, value []byte) {
			newCtx.Posts[string(key)] = string(value)
		})
		// TODO: parse file upload
		newCtx.ApiAction = newCtx.Posts[ApiActionKey]
	} else {
		newCtx.MenuBuffer = bytes.Buffer{}
	}
	return newCtx
}

func (ctx *HttpCtx) QueryString() M.SS {
	res := M.SS{}
	ctx.QueryArgs().VisitAll(func(key, value []byte) {
		res[string(key)] = string(value)
	})
	return res
}

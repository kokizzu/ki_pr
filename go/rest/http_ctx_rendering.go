package rest

import (
	"bytes"
	"github.com/kokizzu/gotro/I"
	"github.com/kokizzu/gotro/L"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/X"
	"github.com/kokizzu/gotro/Z"
	"github.com/valyala/fasthttp"
)

// load view/viewName template
func (ctx *HttpCtx) LoadView(viewName string) *Z.TemplateChain {
	cache := ctx.ViewCache.Get(viewName)
	var tc *Z.TemplateChain
	var ok bool
	tc, ok = cache.(*Z.TemplateChain)
	if cache == nil || tc == nil || !ok || ctx.DebugMode {
		var err error
		viewFile := ctx.ViewDir + viewName + `.html`
		tc, err = Z.ParseFile(ctx.DebugMode, ctx.DebugMode, viewFile)
		L.PanicIf(err, `Layout template not found: `+viewFile)
		ctx.ViewCache.Set(viewName, tc)
	}
	return tc
}

// render html with menus
func (ctx *HttpCtx) RenderHtml(viewName string, locals M.SX) {
	buff := &bytes.Buffer{}
	ctx.LoadView(viewName).Render(buff, locals)
	layoutBuff := &bytes.Buffer{}
	ctx.LayoutCache.Render(layoutBuff, M.SX{
		`title`:   ctx.PageTitle,
		`content`: buff.String(),
		`domain`:  ctx.Domain,
		`project`: ctx.ProjectName,
		`menus`:   ctx.MenuBuffer.String(),
		// TODO: add minified assets
	})
	ctx.SetBodyString(layoutBuff.String())
	ctx.Response.Header.Set("Content-Type", `text/html`)
}

// render json
func (ctx *HttpCtx) RenderJson() {
	ctx.SetBodyString(X.ToJson(ctx.JsonResponse))
	ctx.Response.Header.Set("Content-Type", `application/json`)
}

// render error page
func (ctx *HttpCtx) ErrorHtml(statusCode int, detail string) {
	ctx.Error(fasthttp.StatusMessage(statusCode), statusCode)
	ctx.RenderHtml(I.ToStr(statusCode), M.SX{
		`error_detail`: detail,
	})
}

// render error json
func (ctx *HttpCtx) ErrorJson(statusCode int, detail string) {
	ctx.Error(fasthttp.StatusMessage(statusCode), statusCode)
	ctx.JsonResponse = M.SX{`error`: I.ToStr(statusCode)}
	ctx.RenderJson()
}

// given invalid API
func (ctx *HttpCtx) ApiInvalidAction() {
	ctx.JsonResponse[`error`] = `404 Invalid API action: ` + ctx.ApiAction
}

// get custom segment by name /foo/bar/:baz --> segmentName = `baz`
func (ctx *HttpCtx) ParamStr(segmentName string) string {
	return X.ToS(ctx.UserValue(segmentName))
}
func (ctx *HttpCtx) ParamInt(segmentName string) int64 {
	return X.ToI(ctx.UserValue(segmentName))
}

// generic error response, use model/errors.go
func (ctx *HttpCtx) ApiError(errMsg string) {
	ctx.JsonResponse[`error`] = errMsg
}

// create url for sending e-mail,
func (ctx *HttpCtx) CreateUrl(suffix string) string {
	return string(ctx.Request.URI().Scheme()) + `://` + ctx.Domain + suffix
}

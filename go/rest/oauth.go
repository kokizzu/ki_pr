package rest

import (
	"bitbucket.org/kokizzu/ki_pr/model"
	"github.com/kokizzu/gotro/L"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/S"
	"github.com/kokizzu/gotro/X"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"io/ioutil"
)

var OauthUrls = []string{}
var GoogleOauthProviders map[string]*oauth2.Config
var GoogleUserInfoEndpoint string
var ErrPrefix = model.ERR_612_FAILED_OAUTH

func (ws *WebServer) InitOauth() {
	if ws.DebugMode {
		OauthUrls = append(OauthUrls, `http://`+DevProxyDomain)
		OauthUrls = append(OauthUrls, `http://`+DevDomain+DevListenPort)
		OauthUrls = append(OauthUrls, `http://`+DevIpAddr+DevListenPort)
		OauthUrls = append(OauthUrls, `http://`+DevDomain+DevProxyPort)
		OauthUrls = append(OauthUrls, `http://`+DevIpAddr+DevProxyPort)
	} else {
		OauthUrls = append(OauthUrls, `http://`+ws.Domain)
		OauthUrls = append(OauthUrls, `https://`+ws.Domain)
		OauthUrls = append(OauthUrls, `http://www.`+ws.Domain)
		OauthUrls = append(OauthUrls, `https://www.`+ws.Domain)
	}
	L.Print(OauthUrls)

	GoogleOauthProviders = map[string]*oauth2.Config{}
	for _, url := range OauthUrls {
		GoogleOauthProviders[url] = &oauth2.Config{
			ClientID:     model.GplusClientId,
			ClientSecret: model.GplusClientSecret,
			RedirectURL:  url + model.GoogleOauthCallbackPath,
			Scopes: []string{
				`openid`,
				`email`,
				`profile`,
			},
			Endpoint: google.Endpoint,
		}
	}
}

func RetrieveGoogleUserInfo(provider *oauth2.Config, access_token *oauth2.Token) (M.SX, error) {
	client := provider.Client(oauth2.NoContext, access_token)
	if GoogleUserInfoEndpoint == `` {
		// no need to refetch userinfo_endpoint
		response, err := client.Get(`https://accounts.google.com/.well-known/openid-configuration`)
		if L.IsError(err, ErrPrefix+`RetrieveGoogleUserInfo.GoogleUserInfoEndpoint.Get`) {
			return nil, err
		}
		body, err := ioutil.ReadAll(response.Body)
		response.Body.Close()
		if L.IsError(err, ErrPrefix+`RetrieveGoogleUserInfo.GoogleUserInfoEndpoint.Body`) {
			return nil, err
		}
		json_body := S.JsonToMap(string(body))
		GoogleUserInfoEndpoint = X.ToS(json_body[`userinfo_endpoint`])
	}
	response, err := client.Get(GoogleUserInfoEndpoint)
	if L.IsError(err, ErrPrefix+`RetrieveGoogleUserInfo.Get`) {
		return nil, err
	}
	body, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if L.IsError(err, ErrPrefix+`RetrieveGoogleUserInfo.Body`) {
		return nil, err
	}
	json := S.JsonToMap(string(body)) // example: {"email":"","email_verified":true,"family_name":"","gender":"","given_name":"","locale":"en-GB","name":"","picture":"http://","profile":"http://","sub":"number"};
	return json, nil
}

func GoogleOauthProvider(ctx *HttpCtx) (provider *oauth2.Config, host string) {
	uri := ctx.Request.URI()
	host = string(uri.Scheme()) + `://` + string(uri.Host())
	provider = GoogleOauthProviders[host]
	return
}

func GoogleExchangeInfo(provider *oauth2.Config, code string) (M.SX, error) {
	token, err := provider.Exchange(oauth2.NoContext, code)
	if L.IsError(err, ErrPrefix+`GoogleExchangeInfo.Exchange`) {
		return nil, err
	}
	return RetrieveGoogleUserInfo(provider, token)
}

package rest

import (
	"errors"
	"fmt"
	"github.com/OneOfOne/cmap"
	"github.com/buaazp/fasthttprouter"
	"github.com/fatih/color"
	"github.com/kokizzu/gotro/I"
	"github.com/kokizzu/gotro/L"
	"github.com/kokizzu/gotro/S"
	"github.com/kokizzu/gotro/Z"
	"github.com/valyala/fasthttp"
	"log"
	"runtime"
	"time"
)

const DevListenPort = `:3001`
const DevProxyPort = `:3000`
const DevDomain = `localhost`
const DevIpAddr = `127.0.0.1`
const DevProxyDomain = `local.egaoshark.com`

// WebServer struct, stores global configuration across requests
type WebServer struct {
	*fasthttprouter.Router
	DebugMode   bool
	RootDir     string
	ViewDir     string
	Domain      string
	ViewCache   *cmap.CMap
	PageTitle   string
	LayoutCache *Z.TemplateChain
	ProjectName string
}

// start listen
func (ws *WebServer) StartService(listenPort string) {
	fmt.Println(`WebServer started at http://` + ws.Domain)
	log.Fatal(fasthttp.ListenAndServe(listenPort, ws.Router.Handler))
}

// create WebServer object
func CreateServer(debugMode bool, projectName, domain, rootDir string, routes map[string]ReqHandler) *WebServer {
	webServer := &WebServer{
		DebugMode:   debugMode,
		RootDir:     rootDir,
		ViewDir:     rootDir + `view/`,
		ViewCache:   cmap.New(),
		Domain:      domain,
		ProjectName: projectName,
	}
	webServer.InitOauth()
	webServer.LoadLayout()
	// register all route
	router := fasthttprouter.New()
	for path, handler := range routes {
		path = `/` + path
		wrapper := webServer.ErrorHandler(handler)
		router.GET(path, wrapper)
		router.POST(path, wrapper)
	}
	router.NotFound = fasthttp.FSHandler(rootDir+`public`, 0)
	webServer.Router = router
	return webServer
}

// wrap every request with error handler and logger
func (ws *WebServer) ErrorHandler(originalHandler ReqHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		newCtx := NewHttpCtx(ws, ctx)
		url := string(ctx.Request.RequestURI())
		method := string(ctx.Request.Header.Method())
		postStr := newCtx.Posts.PrettyFunc(` | `, CensorPasswordTrimLongContent)
		if ws.DebugMode {
			L.LOG.Notice(method, url, postStr)
		}
		defer func() {
			err := recover()
			// handle nested error
			defer func() {
				if err2 := recover(); err2 != nil {
					L.Print(`!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!`)
					L.Print(`Nested Error on ErrorHandler (should not be happening)`)
					L.Describe(err)
					L.Describe(err2)
					start := 0
					for {
						pc, file, line, ok := runtime.Caller(start)
						if !ok {
							break
						}
						name := runtime.FuncForPC(pc).Name()
						start += 1
						L.Print("\t" + file + `:` + I.ToStr(line) + `  ` + name)
					}
				}
			}()
			// handle error
			if err != nil {
				err2, ok := err.(error)
				if !ok {
					err2 = errors.New(fmt.Sprintf("%#v", err))
				}
				err_str := err2.Error()
				L.LOG.Errorf(err_str)
				str := L.StackTrace(2)
				L.LOG.Criticalf("StackTrace: %s", str)
				newCtx.PageTitle += ` (error)`
				detail := ``
				if newCtx.DebugMode {
					detail = err_str + string(L.StackTrace(3))
				} else {
					ref_code := `EREF:` + S.RandomCB63(4)
					L.LOG.Notice(ref_code) // no need to print stack trace, should be handled by PanicFilter
					detail = `Detailed error message not available on production mode, error reference code for webmaster: ` + ref_code
					// TODO: queue mail to developer
				}
				if newCtx.IsApiCall {
					newCtx.ErrorJson(fasthttp.StatusInternalServerError, detail)
				} else {
					newCtx.ErrorHtml(fasthttp.StatusInternalServerError, detail)
				}
			}
			// log access
			L.Trace()
			var codeStr string
			code := ctx.Response.StatusCode()
			if code < 400 {
				codeStr = L.BgGreen(`%s`, color.BlueString(`%3d`, code))
			} else {
				codeStr = L.BgRed(`%3d`, code)
			}
			ones := `nano`
			elapsed := float64(time.Since(newCtx.Now).Nanoseconds())
			if elapsed > 1000000000.0 {
				elapsed /= 1000000000.0
				ones = `sec`
			} else if elapsed > 1000000.0 {
				elapsed /= 1000000.0
				ones = `mili`
			} else if elapsed > 1000.0 {
				elapsed /= 1000.0
				ones = `micro`
			}
			referrer := string(ctx.Request.Header.Referer())
			msg := fmt.Sprintf(`[%s] %7d %7.2f %5s | %4s %-40s | %-40s | %15s | %s | %s | %s`,
				codeStr,
				len(ctx.Response.Body()),
				elapsed,
				ones,
				method,
				url,
				referrer,
				ctx.RemoteIP(),
				string(ctx.Request.Header.UserAgent()),
				newCtx.Session.Pretty(` `),
				postStr,
			)
			msg = S.Replace(msg, `%`, `%%`)
			L.LOG.Notice(msg)
		}()
		// forward request to original handler
		originalHandler(newCtx)
	})
}

// load layout for the first time
func (ws *WebServer) LoadLayout() {
	layoutFile := ws.ViewDir + `layout.html`
	tc, err := Z.ParseFile(ws.DebugMode, ws.DebugMode, layoutFile)
	L.PanicIf(err, `Layout template not found: `+layoutFile)
	ws.LayoutCache = tc
}

// censor password and trim long contents
func CensorPasswordTrimLongContent(key, val string) string {
	if len(val) > 64 {
		return val[:64] + `...`
	}
	if S.Contains(key, `pass`) {
		return S.Repeat(`*`, len(val))
	}
	return val
}

package rest

import (
	"bitbucket.org/kokizzu/ki_pr/model"
	"fmt"
	"github.com/kokizzu/gotro/L"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/S"
	"github.com/valyala/fasthttp"
	"sync/atomic"
)

func (ctx *HttpCtx) sessionKey() string {
	return string(ctx.Request.Header.Cookie(model.CookieKey))
}

// load session
func (ctx *HttpCtx) LoadSession() {
	ctx.Session = M.SX{}
	sessionKey := ctx.sessionKey()
	if S.StartsWith(sessionKey, model.SessionRedisPrefix) {
		hmap, _ := model.Session.HGetAll(sessionKey).Result()
		if hmap != nil && len(hmap) > 0 {
			for k, v := range hmap {
				ctx.Session[k] = v
			}
			ctx.Actor = ctx.Session.GetInt(model.ActorSessionKey)
		}
	}
}

// clear session, for logout
func (ctx *HttpCtx) ClearSession() {
	sessionKey := ctx.sessionKey()
	if S.StartsWith(sessionKey, model.SessionRedisPrefix) {
		// delete from redis
		_, err := model.Session.Del(sessionKey).Result()
		L.PanicIf(err, `failed to clear session`)
		// delete from client
		ctx.Response.Header.DelClientCookie(model.CookieKey)
	}
}

// create or update session, must be called after done updating the ctx.Session values
func (ctx *HttpCtx) UpsertSession(updater func(M.SX)) {
	if updater != nil {
		updater(ctx.Session)
		ctx.Actor = ctx.Session.GetInt(model.ActorSessionKey)
	}
	L.Print(ctx.Session)
	sessionKey := ctx.sessionKey()
	expirationDate := ctx.Now.Add(model.SessionCookieLifespan)
	if !S.StartsWith(sessionKey, model.SessionRedisPrefix) {
		// generate totally unique session key
		sessionKey = S.RandomCB63(4) + `:` +
			S.EncodeCB63(ctx.Now.UnixNano(), 1) + `:` +
			S.EncodeCB63(atomic.AddInt64(&model.UniqNumber, 1), 1)
		ctx.Request.Header.SetCookie(model.CookieKey, sessionKey)
		sessionKey = model.SessionRedisPrefix + sessionKey
		fmt.Println(`New session created: ` + sessionKey)
		L.Describe(ctx.Session)
	}
	// save to redis
	_, err := model.Session.HMSet(sessionKey, ctx.Session).Result()
	L.PanicIf(err, `failed to set session`)
	_, err = model.Session.ExpireAt(sessionKey, expirationDate).Result()
	L.IsError(err, `failed to set session expiration date`)
	// write to client
	newCookie := &fasthttp.Cookie{}
	newCookie.SetKey(model.CookieKey)
	newCookie.SetValue(sessionKey)
	newCookie.SetExpire(expirationDate)
	newCookie.SetPath(`/`)
	ctx.Response.Header.SetCookie(newCookie)
}

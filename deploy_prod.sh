#!/usr/bin/env bash

# TODO: --exclude must be added when there are new folder on the server that do not want to be overwritten with local when deploy

alias tcmd='/usr/bin/time -f "CPU: %Us\tReal: %es\tRAM: %MKB"'
alias upx=goupx


if [ "$GOPATH" == "" ]; then
  echo "> GOPATH not set, auto-set to compile-node's GOPATH.."
  GOPATH=/home/`whomai`/go
fi

echo '> Compiling:'

SSH_USER=root
SERVER=egaoshark.com
SSH_PORT=22
WEB_PORT=8888
WEB_USER=web
WEB_GROUP=users
HOME_DIR=/home/${WEB_USER}/egaoshark
PROJECT=KI_PR
BINARY=egaoshark-web

# build server
SERVER_DIR=${SSH_USER}@${SERVER}:${HOME_DIR}
#if [ "$(uname)" == 'Darwin' ]; then
#  echo '> Error: Unable deploy from MacOSX'
#  exit 10
#fi
BUILD_DATE=`date +.%Y%m%d.%H%M%S`

PIDS=''

export GOOS=linux
export GOARCH=amd64

echo '> Building KI_PR Web and API Server..' \
&& go build -ldflags "
	-X main.ListenAddr=:${WEB_PORT}
	-X main.BuildDate=${BUILD_DATE}
	-X main.ProjectName=${PROJECT}
	-X main.Domain=${SERVER}
" -o /tmp/${BINARY} \
&& cp /tmp/${BINARY} /tmp/${BINARY}-raw & 
PIDS="$PIDS $!"

for pid in $PIDS; do
	wait $pid || let 'fail=1'
done

if [ "$fail" == '1' ]; then
	echo '> One or more build process failed..'
	exit 11
fi

EXECUTABLES=''
REMOTE_CMD="systemctl restart ${BINARY}; "
# ^ always restart

PIDS=''

echo '> Comparing binaries..' \

b_diff=`cmp -l /tmp/${BINARY}-raw /tmp/${BINARY}-old | wc -l`
if [ "$b_diff" -gt 6 ] || [ "$b_diff" == 0 ]; then
	EXECUTABLES="$EXECUTABLES /tmp/${BINARY}"
	echo "> ${binary} ${b_diff} bytes differ, compressing.." 
	upx --no-progress /tmp/${BINARY} &
	PIDS="$PIDS $!"
fi

if [ "${EXECUTABLES}" != '' ] ; then
	echo ${BUILD_DATE} > public/last_deploy
	echo `git log -n 1 | head -n 4` >> public/last_deploy
#	echo '> Generating API docs..'
#	unset GOOS
#	unset GOARCH
#	pushd . &&
#	cd go/apidocs &&
#	go run gen_apidoc.go &&
#	popd
	PIDS="$PIDS $!"
fi

wait ${PIDS}

echo "> Moving executables and scripts.. ${EXECUTABLES}" \
&& rsync -L -h -t -P -e "ssh -p ${SSH_PORT}" run_*.sh ${EXECUTABLES} ${SERVER_DIR} \
&& echo '> Sychronizing current release..' \
&& rsync -L -h -t -P -r --delete -e "ssh -p ${SSH_PORT}" \
--exclude '.*' \
--exclude 'public/pictures/*' \
--exclude 'public/videos/*' \
--exclude 'public/pictures' \
--exclude 'public/videos' \
--exclude 'public/js/all.js' \
--exclude 'public/css/all.css' \
--exclude 'public/js/mod.js' \
--exclude 'public/css/mod.css' \
--exclude 'public/js/lib.js' \
--exclude 'public/css/lib.css' \
--exclude 'logs' \
--exclude 'go' \
--exclude '*.sql' \
--exclude 'logs/*' \
--exclude "${BINARY}" \
--exclude 'ki_pr*' \
--exclude 'gin-bin' \
--exclude 'controller' \
--exclude 'model' \
--exclude 'sql*' \
--exclude 'tmp' \
--exclude '*.go' \
--exclude '*.sh' \
--exclude '*.md' \
--exclude '*.txt' \
--exclude '*.iml' \
--exclude '*.java' \
--exclude '*.log' \
--exclude '*.service' \
--exclude 'Caddyfile' \
. ${SERVER_DIR} \
&& echo "> Executing remote commands.. ${REMOTE_CMD}" \
&& ssh ${SSH_USER}@${SERVER} -p ${SSH_PORT} "
chown -R ${WEB_USER}:${WEB_GROUP} ${HOME_DIR} ; 
${REMOTE_CMD}" \
&& mv /tmp/${BINARY}-raw /tmp/${BINARY}-old
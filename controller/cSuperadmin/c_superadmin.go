package cSuperadmin

import (
	"bitbucket.org/kokizzu/ki_pr/controller"
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	"bitbucket.org/kokizzu/ki_pr/model/mUser"
	"github.com/kokizzu/gotro/M"
)

func Users(ctx *rest.HttpCtx) {
	if controller.InvalidRole(`Users`, ctx) {
		return
	}
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `create`, `update`, `delete`, `restore`: // @API 2019-07-05
			mUser.API_CreateUpdateDeleteRestore(ctx)
		case `list`: // @API 2019-07-05
			mUser.API_List(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	ctx.RenderHtml(`superadmin/users`, M.SX{})
}

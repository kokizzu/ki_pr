package controller

import (
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	"bitbucket.org/kokizzu/ki_pr/model"
	"bytes"
	"github.com/kokizzu/gotro/M"
)

// should not collide with *SessionKey
const guestSegment = `guest`

var Menus = []string{guestSegment, `my`, `superadmin`}

// generate menu
func GenerateMenu(pageTitle string, ctx *rest.HttpCtx) {
	ctx.PageTitle = pageTitle
	subMenu := bytes.Buffer{}
	loggedIn := ctx.Actor > 0
	for _, menu := range Menus {
		if menu == `guest` && !loggedIn ||
			loggedIn && ctx.Session.GetInt(menu) > 0 {
			ctx.LoadView(`menu/`+menu).Render(&subMenu, M.SX{})
		}
	}
	ctx.LoadView(`menu/index`).Render(&ctx.MenuBuffer, M.SX{
		`project`: ctx.ProjectName,
		`title`:   pageTitle,
		`submenu`: subMenu,
		`email`:   ctx.Session.GetStr(model.EmailSessionKey),
	})
}

// check if first segment allowed to be accessed by this logged user, also renders menu if user can access the page
func InvalidRole(pageTitle string, ctx *rest.HttpCtx) bool {
	ctx.PageTitle = pageTitle
	if !(ctx.FirstSegment == `` || ctx.FirstSegment == guestSegment) &&
		ctx.Session.GetInt(ctx.FirstSegment) == 0 {
		errMsg := `Insufficient privilege to access segment: /` + ctx.FirstSegment
		if ctx.IsApiCall {
			ctx.ErrorJson(403, errMsg)
		} else {
			ctx.ErrorHtml(403, errMsg)
		}
		return true
	}
	if !ctx.IsApiCall {
		GenerateMenu(pageTitle, ctx)
	}
	return false
}

func Index(ctx *rest.HttpCtx) {
	if InvalidRole(`Home`, ctx) {
		return
	}
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	ctx.RenderHtml(`index`, M.SX{})
}

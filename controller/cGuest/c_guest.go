package cGuest

import (
	"bitbucket.org/kokizzu/ki_pr/controller"
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	"bitbucket.org/kokizzu/ki_pr/model/mUser"
	"github.com/kokizzu/gotro/M"
)

func Login(ctx *rest.HttpCtx) {
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `login`: // @API 2019-07-05
			mUser.API_Login(ctx)
		case `login_url`: // @API 2019-07-05
			mUser.API_LoginUrl(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	mUser.API_LoginUrl(ctx)
	controller.GenerateMenu(`Login`, ctx)
	ctx.RenderHtml(`guest/login`, ctx.JsonResponse)
}

func Register(ctx *rest.HttpCtx) {
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `register`: // @API 2019-07-05
			mUser.API_Register(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	mUser.API_LoginUrl(ctx)
	controller.GenerateMenu(`Register`, ctx)
	ctx.RenderHtml(`guest/register`, ctx.JsonResponse)
}

func ForgotPassword(ctx *rest.HttpCtx) {
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `forgot_password`: // @API 2019-07-05
			mUser.API_ForgotPassword(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	controller.GenerateMenu(`Forgot Password`, ctx)
	ctx.RenderHtml(`guest/forgot_password`, M.SX{})
}

func ResetPassword(ctx *rest.HttpCtx) {
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `reset_password`: // @API 2019-07-05
			mUser.API_ResetPassword(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	controller.GenerateMenu(`Reset Password`, ctx)
	ctx.RenderHtml(`guest/reset_password`, M.SX{})
}

func VerifyEmail(ctx *rest.HttpCtx) {
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `verify_email`: // @API 2019-07-05
			mUser.API_VerifyEmail(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	mUser.API_VerifyEmail(ctx)
	controller.GenerateMenu(`E-Mail Verification`, ctx)
	ctx.RenderHtml(`guest/verify_email`, ctx.JsonResponse)
}

func GoogleLogin(ctx *rest.HttpCtx) {
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `google_login`: // @API 2019-07-05
			mUser.API_GoogleLogin(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	mUser.API_GoogleLogin(ctx)
	controller.GenerateMenu(`Google Login`, ctx)
	ctx.RenderHtml(`guest/google_login`, ctx.JsonResponse)
}

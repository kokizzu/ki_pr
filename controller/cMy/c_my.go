package cMy

import (
	"bitbucket.org/kokizzu/ki_pr/controller"
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	"bitbucket.org/kokizzu/ki_pr/model/mUser"
)

func Profile(ctx *rest.HttpCtx) {
	if controller.InvalidRole(`My Profile`, ctx) {
		return
	}
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `update_profile`: // @API 2019-07-05 including change e-mail and password
			mUser.API_UpdateProfile(ctx)
		case `resend_verification_email`: // @API 2019-07-05
			mUser.API_ResendVerificationEmail(ctx)
		case `my_info`: // @API 2019-07-07
			mUser.API_MyInfo(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	mUser.API_MyInfo(ctx)
	ctx.RenderHtml(`my/profile`, ctx.JsonResponse)
}

func Logout(ctx *rest.HttpCtx) {
	if controller.InvalidRole(`My Profile`, ctx) {
		return
	}
	if ctx.IsApiCall {
		switch ctx.ApiAction {
		case `logout`: // @API 2019-07-05
			mUser.API_Logout(ctx)
		default: // @API-END
			ctx.ApiInvalidAction()
		}
		ctx.RenderJson()
		return
	}
	mUser.API_Logout(ctx)
	ctx.MenuBuffer.Reset()
	ctx.Redirect(`/`, 302)
}

package main

import (
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	"bitbucket.org/kokizzu/ki_pr/model"
	"fmt"
	"github.com/kokizzu/gotro/S"
	"os"
	"runtime"
)

//import "github.com/pkg/profile"

var ListenAddr = rest.DevListenPort // since autoreloader uses :3000, replaced when building production binary
var BuildDate = ``
var ProjectName = `KI_PR` // should not contain invalid character for mailing (eg. <, >)
var Domain = rest.DevDomain
var RootDir string

func init() {
	var err error
	RootDir, err = os.Getwd() // filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		_, path, _, _ := runtime.Caller(0)
		slash_pos := S.LastIndexOf(path, `/`) + 1
		RootDir = path[:slash_pos]
	} else {
		RootDir += `/`
	}
	fmt.Println(`RootDir: ` + RootDir)
}

func main() {
	//defer profile.Start(profile.CPUProfile).Stop()
	model.InitMailer(ProjectName)
	debugMode := Domain == rest.DevDomain
	if debugMode { // remove this if using reverse proxy (caddy, envoy, nginx, etc) for development
		Domain += ListenAddr
	}
	model.AutoMigrateEnableQueryLog(debugMode)
	server := rest.CreateServer(debugMode, ProjectName, Domain, RootDir, ROUTES)
	server.StartService(ListenAddr)
}

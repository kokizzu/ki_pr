# KI_PR

## dependencies

- fastest http router: github.com/buaazp/fasthttprouter
- live-reload utility for development: github.com/codegangsta/gin
- ORM: github.com/jinzhu/gorm
- MySQL driver: github.com/go-sql-driver/mysql
- Redis driver for storing session: github.com/go-redis/redis
- Javascript-compatible template engine: github.com/kokizzu/gotro/Z
- concurrent map: github.com/OneOfOne/cmap
- map functions and alias: github.com/kokizzu/gotro/M
- string functions and alias: github.com/kokizzu/gotro/S
- integer functions and alias: github.com/kokizzu/gotro/I
- logger functions: github.com/kokizzu/gotro/L
- interface conversion functions: github.com/kokizzu/gotro/X
- Google oauth2 library: golang.org/x/oauth2/google 
- adminlte.io template for web UI
- mailer library: github.com/jordan-wright/email
 
## routes

All route defined as `/segment1/segment2/...`, where `segment1` is the role (eg. guest, me/profile, admin, sales, support, billing, marketing, etc), and `segment2` is the feature (eg. users, products, inventory, shipment, orders, etc). All request using GET will return an HTML, so all rest API must use POST (no DELETE and PUT because certain client's old browser/proxy/corporate firewall does not allow or support this http method).

## project structure

- `handler/` controllers, eg. `/guest/login` will be handled by `hGuest.Login` procedure, all routes listed in `routes.go` 
- `model/` each table and configuration (including database connections)
- `view/` view used by web client, must equal to the route path, eg. `/guest/login` must use `view/guest/login.html` template
- `public/` all public resource for web client

only `view/`, `public/`, and the binary needed to be deployed on the server.

## preparation

- install dependencies (mysql, redis)
- copy secret.go.example to secret.go, modify as needed

## deployment

- create systemd service file
- run `./deploy_prod.sh`
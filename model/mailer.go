package model

import (
	"github.com/jordan-wright/email"
	"github.com/kokizzu/gotro/A"
	"github.com/kokizzu/gotro/I"
	"github.com/kokizzu/gotro/L"
	"github.com/kokizzu/gotro/S"
	"net/smtp"
)

type SmtpConfig struct {
	Name          string
	Username      string
	Password      string
	Hostname      string
	Port          int
	SubjectPrefix string
}

func (mc *SmtpConfig) Address() string {
	return mc.Hostname + `:` + I.ToStr(mc.Port)
}

func (mc *SmtpConfig) Auth() smtp.Auth {
	return smtp.PlainAuth(``, mc.Username, mc.Password, mc.Hostname)
}
func (mc *SmtpConfig) From() string {
	return mc.Name + ` <` + mc.Username + `>`
}

// run sendbcc on another goroutine
func (mc *SmtpConfig) SendBCC(bcc []string, subject string, message string) {
	go mc.SendSyncBCC(bcc, subject, message)
}

// run sendAttachbcc on another goroutine
func (mc *SmtpConfig) SendAttachBCC(bcc []string, subject string, message string, files []string) {
	go mc.SendSyncAttachBCC(bcc, subject, message, files)
}

// sendbcc synchronous version, returns error message
func (mc *SmtpConfig) SendSyncBCC(bcc []string, subject string, message string) string {
	return mc.SendSyncAttachBCC(bcc, subject, message, []string{})
}

// sendbcc synchronous version, returns error message
func (mc *SmtpConfig) SendSyncAttachBCC(bcc []string, subject string, message string, files []string) string {
	L.Print(`SendSyncAttachBCC started ` + A.StrJoin(bcc, `, `) + `; subject: ` + subject)
	e := email.NewEmail()
	e.From = mc.From()
	e.To = []string{e.From}
	e.Bcc = bcc
	e.Subject = mc.SubjectPrefix + subject
	attach := A.StrJoin(files, ` `)
	for _, file := range files {
		e.AttachFile(file)
	}
	if attach != `` {
		attach = `; attachments: ` + attach
	}

	e.HTML = []byte(S.Replace(message, "\n", `<br/>`) + `<br/>
<br/>
--<br/>
Sincerely,<br/>
Automated Software<br/>
` + e.From)
	L.Describe(e.Subject, e.Bcc)
	err := e.Send(mc.Address(), mc.Auth())
	if L.IsError(err, `failed to SendSyncAttachBCC`) {
		return err.Error()
	}
	L.Print(`SendSyncAttachBCC completed ` + A.StrJoin(bcc, `, `) + attach + `; subject: ` + subject)
	return ``
}

func InitMailer(projectName string) {
	Mailer.SubjectPrefix = `[` + projectName + `] `
	Mailer.Name = projectName + ` Mailer`
}

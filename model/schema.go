package model

import (
	"github.com/jinzhu/gorm"
	"github.com/kokizzu/gotro/S"
	"hash/adler32"
	"sync/atomic"
	"time"
)

type User struct {
	gorm.Model
	Email      string     `gorm:"type:varchar(64);unique_index" json:"email"`
	Password   string     `gorm:"type:varbinary(64)" json:"-"`
	Address    string     `gorm:"type:varchar(256)" json:"address"`
	FullName   string     `gorm:"type:varchar(64)" json:"full_name"`
	Phone      string     `gorm:"type:varchar(24)" json:"phone"`
	Latitude   float64    `json:"latitude"`
	Longitude  float64    `json:"longitude"`
	VerifiedAt *time.Time `json:"verified_at"` // email verified at
	UpdatedBy  int64      `json:"updated_by"`
}

const User_Email = `email`
const User_Password = `password`
const User_Address = `address`
const User_FullName = `full_name`
const User_Phone = `phone`
const User_Latitude = `latitude`
const User_Longitude = `longitude`
const User_VerifiedAt = `verified_at`

var userAutoIncrement uint64
var Product = &struct{ Memsql uint64 }{}

func (u *User) AfterSave() error {
	// TODO: log creation/changes to append-only database
	return nil
}

func (u *User) AfterCreate(tx *gorm.DB) (err error) {
	if u.ID == 0 { // workaround for memsql
		nextId := atomic.AddUint64(&userAutoIncrement, 1)
		if tx.Exec(`UPDATE users SET id = ? WHERE email = ? LIMIT 1`, nextId, u.Email).RowsAffected == 1 {
			u.ID = uint(nextId)
		} else {
			tx.Rollback()
		}
	}
	return
}

func (u *User) VerificationCode() string {
	val := int64(adler32.Checksum([]byte(u.Email)))
	return S.EncodeCB63(val, 1)
}

func Q(str string) string {
	return "`" + str + "`"
}

func AutoMigrateEnableQueryLog(debugMode bool) {
	_ = Database.Raw(`SELECT COUNT(*) "memsql" FROM information_schema.global_variables WHERE variable_value LIKE '%memsql%'`).Scan(Product)
	Database.LogMode(debugMode)
	if !Database.HasTable(&User{}) && Product.Memsql > 0 {
		Database.Exec(`
CREATE TABLE IF NOT EXISTS ` + Q(`users`) + ` (
	` + Q(`id`) + ` int unsigned
,` + Q(`created_at`) + ` timestamp NULL
,` + Q(`updated_at`) + ` timestamp NULL
,` + Q(`deleted_at`) + ` timestamp NULL
,` + Q(`email`) + ` varchar(64)
,` + Q(`password`) + ` varbinary(64)
,` + Q(`address`) + ` varchar(256)
,` + Q(`full_name`) + ` varchar(64)
,` + Q(`phone`) + ` varchar(24)
,` + Q(`latitude`) + ` double
,` + Q(`longitude`) + ` double
,` + Q(`verified_at`) + ` timestamp NULL
,` + Q(`updated_by`) + ` bigint 
, UNIQUE uix_users_email (` + Q(`email`) + `)
, SHARD KEY(` + Q(`email`) + `)
)`)
	}
	Database.AutoMigrate(&User{})
	if Product.Memsql > 0 {
		_ = Database.Raw(`SELECT IFNULL(MAX(id),0) "memsql" FROM users`).Scan(Product)
		userAutoIncrement = Product.Memsql
	}
}

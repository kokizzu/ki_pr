package mUser

import (
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	. "bitbucket.org/kokizzu/ki_pr/model"
	"github.com/kokizzu/gotro/F"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/S"
)

// returns user by email, if wrong password the ID is 0
func One_ByEmailPassword(email, rawPassword string) *User {
	user := &User{}
	Database.First(user, `email = ?`, email)
	if user.Email == email && CheckPassword(user.Password, rawPassword) != nil {
		user.ID = 0
	}
	return user
}

// send verification email
func Send_VerificationEmail(ctx *rest.HttpCtx, user *User) {
	redisKey := VerifyLinkRedisPrefix + user.Email
	if dur, _ := Session.TTL(redisKey).Result(); dur > 0 {
		ctx.ApiError(ERR_616_VERIFY_LINK_REQUESTED_TOO_SOON + F.ToStr(dur.Minutes()))
		return
	}
	emailCode := user.VerificationCode()
	uid := S.EncodeCB63(int64(user.ID), 1)
	verifyUrl := ctx.CreateUrl(`/guest/verify_email/` + emailCode + `/` + uid)
	Session.Set(redisKey, user.ID, VerifyEmailLinkThrottle).Result()
	Mailer.SendBCC([]string{user.Email}, MailSubject_EmailVerification, `
Hi `+user.Email+`, 

We received a registration for `+ctx.ProjectName+` using your email.
To verify your e-mail, please click the following link:

`+verifyUrl+`

If you did not initiate this request, you may safely ignore this message.
`)
}

// insert or update user
func Upsert(ctx *rest.HttpCtx, user *User) {
	u := NewUpdater(ctx.Posts, ctx.Actor, ctx.Now)
	// change password
	newPass := ctx.Posts.GetStr(`password`)
	if newPass != `` {
		u.SetRaw(User_Password, HashPassword(newPass))
	}
	// change info
	u.SetStr(User_FullName)
	u.SetStr(User_Address)
	u.SetPhone(User_Phone)
	u.SetFloat(User_Latitude)
	u.SetFloat(User_Longitude)
	// change email
	newEmail := ctx.Posts.GetStr(`email`)
	invalidateEmail := false
	if newEmail != `` {
		newEmail = S.ValidateEmail(newEmail)
		newEmail = S.ToLower(newEmail)
		if newEmail != `` && newEmail != user.Email {
			u.SetRaw(User_Email, newEmail)
			u.SetNull(User_VerifiedAt)
			invalidateEmail = true
		}
	}
	// delete/restore
	u.DeleteRestore(ctx.ApiAction)
	// execute update or insert
	affected := int64(0)
	if invalidateEmail && Product.Memsql > 0 { // memsql workaround
		prevId := user.ID
		prevEmail := user.Email
		Database.Exec(`DELETE FROM users WHERE id = ? LIMIT 1`, user.ID)
		Database.FirstOrInit(user, u.Fields)
		user.ID = prevId
		affected = Database.Create(user).RowsAffected
		if affected == 0 {
			user.ID = prevId
			user.Email = prevEmail
			affected = Database.Create(user).RowsAffected
			ctx.ApiError(ERR_603_EMAIL_ALREADY_USED)
			return
		}
	}
	if user.ID == 0 {
		Database.FirstOrInit(user, u.Fields)
		affected = Database.Create(user).RowsAffected
		invalidateEmail = true
	} else {
		affected = u.Update(user).RowsAffected
		if affected == 0 && invalidateEmail {
			ctx.ApiError(ERR_603_EMAIL_ALREADY_USED)
			return
		}
	}
	ctx.JsonResponse.Set(`affected`, affected)
	if affected > 0 && invalidateEmail {
		// resend verification
		Send_VerificationEmail(ctx, user)
	}
}

// update login session
func UpdateLoginSession(ctx *rest.HttpCtx, user *User) {
	ctx.UpsertSession(func(s M.SX) {
		s.Set(ActorSessionKey, user.ID)
		s.Set(EmailSessionKey, user.Email)
		// give access to pages
		s.Set(`my`, 1)
		// temporary hardcoded
		if user.Email == `kiswono@gmail.com` {
			s.Set(`superadmin`, 1)
		}
	})
}

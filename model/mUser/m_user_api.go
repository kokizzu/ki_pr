package mUser

import (
	"bitbucket.org/kokizzu/ki_pr/go/rest"
	. "bitbucket.org/kokizzu/ki_pr/model"
	"errors"
	"github.com/kokizzu/gotro/F"
	"github.com/kokizzu/gotro/I"
	"github.com/kokizzu/gotro/L"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/S"
	"github.com/kokizzu/gotro/X"
)

func API_Login(ctx *rest.HttpCtx) {
	email := ctx.Posts.GetStr(`email`)
	email = S.ValidateEmail(email)
	email = S.ToLower(email)
	pass := ctx.Posts.GetStr(`password`)
	user := One_ByEmailPassword(email, pass)
	if user.ID == 0 {
		ctx.ApiError(ERR_600_WRONG_USERNAME_OR_PASSWORD)
		return
	}
	// save to redis
	UpdateLoginSession(ctx, user)
	// give response
	ctx.JsonResponse.Set(`user`, user)
}

func API_Register(ctx *rest.HttpCtx) {
	email := ctx.Posts.GetStr(`email`)
	full_name := ctx.Posts.GetStr(`full_name`)
	pass := ctx.Posts.GetStr(`pass`)
	email = S.ValidateEmail(email)
	email = S.ToLower(email)
	if email == `` {
		ctx.ApiError(ERR_602_INVALID_EMAIL)
		return
	}
	user := &User{
		Email:    email,
		FullName: S.XSS(full_name),
		Password: HashPassword(pass),
	}
	// save to mysql
	Database.Create(user)
	// give response
	if user.ID == 0 {
		ctx.ApiError(ERR_603_EMAIL_ALREADY_USED)
		return
	}
	// auto login
	UpdateLoginSession(ctx, user)
	ctx.JsonResponse.Set(`user`, user)
	Send_VerificationEmail(ctx, user)
}

func API_ForgotPassword(ctx *rest.HttpCtx) {
	email := ctx.Posts.GetStr(`email`)
	email = S.ValidateEmail(email)
	email = S.ToLower(email)
	if email == `` {
		ctx.ApiError(ERR_609_EMAIL_REQUIRED)
		return
	}
	user := &User{}
	Database.First(user, `email = ?`, email)
	if user.ID == 0 {
		ctx.ApiError(ERR_604_EMAIL_NOT_FOUND)
		return
	}
	// check already sent
	user_id := int64(user.ID)
	redisKey := ForgotPassRedisPrefix + I.ToS(user_id)
	dur, _ := Session.TTL(redisKey).Result()
	if dur > 0 {
		ctx.ApiError(ERR_611_RESET_PASS_LINK_SENT_EXPIRED_IN + F.ToStr(dur.Minutes()))
		return
	}
	randomCode := S.RandomCB63(3)
	// save to session
	deadline := ctx.Now.Add(ForgotPassLifespan)
	_, err := Session.Set(redisKey, randomCode, ForgotPassLifespan).Result()
	L.PanicIf(err, ERR_605_FAIL_SENDING_RESET_PASSWORD_LINK+`: session`)
	uid := S.EncodeCB63(user_id, 4)
	resetUrl := ctx.CreateUrl(`/guest/reset_password/` + randomCode + `/` + uid)
	ctx.JsonResponse.Set(`deadline`, deadline)
	Mailer.SendBCC([]string{user.Email}, MailSubject_ResetPasswordLink, `
Hi `+user.Email+`, 

We received a request to send reset password link for `+ctx.ProjectName+`
To reset your `+ctx.ProjectName+` password, please click the following link:

`+resetUrl+`

The link above valid until `+deadline.UTC().String()+`

If you did not initiate this request, you may safely ignore this message.
`)
}

func API_ResetPassword(ctx *rest.HttpCtx) {
	code := ctx.ParamStr(`code`)
	dec, ok := S.DecodeCB63(ctx.ParamStr(`uid`))
	if !ok {
		ctx.ApiError(ERR_606_RESET_LINK_INVALID_OR_EXPIRED + `: uid` + I.ToS(dec))
		return
	}
	user_id := uint(dec)
	pass := ctx.Posts.GetStr(`pass`)
	user := &User{}
	Database.First(user, `id = ?`, user_id)
	uid := X.ToS(user_id)
	if user.ID != user_id {
		ctx.ApiError(ERR_606_RESET_LINK_INVALID_OR_EXPIRED + `: user` + uid)
		return
	}
	// check redis
	redisKey := ForgotPassRedisPrefix + uid
	randomCode, err := Session.Get(redisKey).Result()
	if err != nil || randomCode != code {
		L.Print(`key|randomCode != code: ` + redisKey + `|` + randomCode + ` != ` + code)
		ctx.ApiError(ERR_606_RESET_LINK_INVALID_OR_EXPIRED + `: ` + err.Error())
		return
	}
	// update record
	u := NewUpdater(ctx.Posts, ctx.Actor, ctx.Now)
	u.SetRaw(User_Password, HashPassword(pass))
	u.SetNow(User_VerifiedAt)
	affected := u.Update(user).RowsAffected
	ctx.JsonResponse.Set(`affected`, affected)
	// unset redis
	Session.Del(redisKey).Result()
	// auto login
	UpdateLoginSession(ctx, user)
}

func API_VerifyEmail(ctx *rest.HttpCtx) {
	code := ctx.ParamStr(`code`)
	dec, ok := S.DecodeCB63(ctx.ParamStr(`uid`))
	if !ok {
		ctx.ApiError(ERR_607_INVALID_VERIFICATION_LINK + `: uid` + I.ToS(dec))
		return
	}
	user_id := uint(dec)
	user := &User{}
	Database.First(user, `id = ?`, user_id)
	if user.ID == 0 {
		ctx.ApiError(ERR_607_INVALID_VERIFICATION_LINK + `: user` + I.ToS(dec))
		return
	}
	if user.VerifiedAt != nil {
		ctx.ApiError(ERR_607_INVALID_VERIFICATION_LINK + `: already verified`)
		return
	}

	emailCode := user.VerificationCode()
	if emailCode != code {
		L.Print(`emailCode <> code`, emailCode, code)
		ctx.ApiError(ERR_607_INVALID_VERIFICATION_LINK + `: code`)
		return
	}
	u := NewUpdater(ctx.Posts, ctx.Actor, ctx.Now)
	u.SetNow(User_VerifiedAt)
	affected := u.Update(user).RowsAffected
	// auto login
	UpdateLoginSession(ctx, user)
	ctx.JsonResponse.Set(`affected`, affected)
	//ctx.JsonResponse.Set(`user`,user)
}

func API_UpdateProfile(ctx *rest.HttpCtx) {
	user := &User{}
	user_id := ctx.Actor
	Database.First(user, `id = ?`, user_id)
	if user.ID == 0 {
		ctx.ApiError(ERR_610_MISSING_USER + I.ToS(user_id))
		return
	}
	// request old password (if any) if change email/password intended
	newPass := ctx.Posts.GetStr(`password`)
	newEmail := ctx.Posts.GetStr(`email`)
	pass := ctx.Posts.GetStr(`pass`)
	if (newPass != `` || newEmail != user.Email) && user.Password != `` && CheckPassword(user.Password, pass) != nil {
		ctx.ApiError(ERR_608_INCORRECT_OLD_PASSWORD)
		return
	}
	Upsert(ctx, user)
	ctx.JsonResponse.Set(`user`, user)
	// TODO: add maps on UI
}

func API_MyInfo(ctx *rest.HttpCtx) {
	user := &User{}
	user_id := ctx.Actor
	Database.First(user, `id = ?`, user_id)
	if user.ID == 0 {
		ctx.ApiError(ERR_610_MISSING_USER + I.ToS(user_id))
		return
	}
	ctx.JsonResponse.Set(`user`, user)
}

func API_ResendVerificationEmail(ctx *rest.HttpCtx) {
	user := &User{}
	Database.First(user, `id = ?`, ctx.Actor)
	Send_VerificationEmail(ctx, user)
	ctx.JsonResponse.Set(`ok`, true)
}

func API_Logout(ctx *rest.HttpCtx) {
	ctx.ClearSession()
}

func API_CreateUpdateDeleteRestore(ctx *rest.HttpCtx) {
	user := &User{}
	id := ctx.Posts.GetInt(`id`)
	Database.First(user, `id = ?`, id)
	email := ctx.Posts.GetStr(`email`)
	email = S.ValidateEmail(email)
	email = S.ToLower(email)
	if user.ID == 0 {
		if email == `` {
			ctx.ApiError(ERR_609_EMAIL_REQUIRED)
			return
		}
		user.Email = email
	}
	Upsert(ctx, user)
}

func API_List(ctx *rest.HttpCtx) {
	// TODO: limit, offset, filter/where, order all in single json object as string
	// Database.Raw()
}

func API_GoogleLogin(ctx *rest.HttpCtx) {
	csrf := ctx.RequestCtx.RemoteIP().String()
	qparams := ctx.QueryString()
	ncsrf := qparams.GetStr(`state`)
	var err error
	if ncsrf != csrf {
		err = errors.New(ERR_613_INVALID_CSRF_STATE + ncsrf + ` <> ` + csrf)
	} else {
		var json M.SX
		code := qparams.GetStr(`code`)
		g_provider, host := rest.GoogleOauthProvider(ctx)
		if g_provider == nil {
			ctx.ApiError(ERR_614_NO_OAUTH_PROVIDER + host)
			return
		}
		json, err = rest.GoogleExchangeInfo(g_provider, code)
		// example: {"email":"","email_verified":true,"family_name":"","gender":"","given_name":"","locale":"en-GB","name":"","picture":"http://","profile":"http://","sub":"number"};
		if err != nil {
			ctx.ApiError(ERR_612_FAILED_OAUTH + err.Error())
			return
		}
		email := json.GetStr(`email`)
		email = S.ValidateEmail(email)
		email = S.ToLower(email)
		if email == `` {
			ctx.ApiError(ERR_615_NO_EMAIL_FROM_OAUTH_PROVIDER)
			return
		}
		user := &User{}
		Database.First(user, `email = ?`, email)
		affected := int64(0)
		if user.ID == 0 {
			user.Email = email
			user.FullName = json.GetStr(`name`)
			affected = Database.Create(user).RowsAffected
		}
		UpdateLoginSession(ctx, user)
		ctx.JsonResponse.Set(`affected`, affected)
		ctx.JsonResponse.Set(`oauth_data`, json)
		ctx.JsonResponse.Set(`user`, user)
		ctx.JsonResponse.Set(`error`, ``)
	}
}

func API_LoginUrl(ctx *rest.HttpCtx) {
	g_provider, host := rest.GoogleOauthProvider(ctx)
	L.Print(host)
	if g_provider != nil {
		csrf := ctx.RequestCtx.RemoteIP().String()
		L.Print(csrf)
		L.Print(g_provider.AuthCodeURL(csrf))
		ctx.JsonResponse.Set(`google_oauth_url`, g_provider.AuthCodeURL(csrf))
	}
}

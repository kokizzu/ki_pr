package mUser

import (
	"bitbucket.org/kokizzu/ki_pr/model"
	"github.com/kokizzu/gotro/L"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(s string) string {
	saltedBytes := []byte(s)
	hashedBytes, err := bcrypt.GenerateFromPassword(saltedBytes, bcrypt.DefaultCost)
	L.PanicIf(err, model.ERR_601_FAILED_TO_HASH_PASSWORD)

	return string(hashedBytes[:])
}

func CheckPassword(hash string, rawPassword string) error {
	incoming := []byte(rawPassword)
	existing := []byte(hash)
	return bcrypt.CompareHashAndPassword(existing, incoming)
}

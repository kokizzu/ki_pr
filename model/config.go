package model

import "time"

// public configuration goes here

const SessionCookieLifespan = 7 * 24 * time.Hour
const ForgotPassLifespan = 15 * time.Minute
const VerifyEmailLinkThrottle = 3 * time.Minute

const CookieKey = `track1`
const SessionRedisPrefix = `s3s:`
const VerifyLinkRedisPrefix = `ver:`
const ForgotPassRedisPrefix = `4got:`
const ActorSessionKey = `actor`
const EmailSessionKey = `email`

const MailSubject_ResetPasswordLink = `Reset Password Link`
const MailSubject_EmailVerification = `E-Mail Verification`

const GoogleOauthCallbackPath = `/guest/google_login`

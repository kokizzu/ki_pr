package model

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/kokizzu/gotro/I"
	"github.com/kokizzu/gotro/L"
	"math/rand"
	"time"
)

var Session *redis.Client
var Database *gorm.DB
var UniqNumber int64

func init() {
	// random number
	rand.Seed(time.Now().Unix())
	UniqNumber = rand.Int63()

	// connect to session database
	Session = redis.NewClient(SessionConfig)
	pong, err := Session.Ping().Result()
	L.PanicIf(err, `failed to connect to Redis: `+SessionConfig.Addr)
	fmt.Println(`Session database: ` + SessionConfig.Addr + ` DB:` + I.ToStr(SessionConfig.DB) + ` ` + pong)

	// connect to mysql
	Database, err = gorm.Open("mysql", MySqlConnParam)
	L.PanicIf(err, `failed to connect to MySQL`)
}

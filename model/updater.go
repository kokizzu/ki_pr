package model

import (
	"github.com/jinzhu/gorm"
	"github.com/kokizzu/gotro/M"
	"github.com/kokizzu/gotro/S"
	"time"
)

type Updater struct {
	Changed bool
	Fields  map[string]interface{}
	Posts   M.SS
	Actor   int64
	Now     time.Time
}

const Field_DeletedAt = `deleted_at`
const Field_UpdatedBy = `updated_by`

var Null = gorm.Expr(`NULL`)

func NewUpdater(posts M.SS, actor int64, now time.Time) *Updater {
	return &Updater{
		Changed: false,
		Fields:  map[string]interface{}{},
		Posts:   posts,
		Actor:   actor,
		Now:     now,
	}
}

// to GORM, do not call Database.Model(model).x manually
func (u *Updater) Update(target interface{}) *gorm.DB {
	if u.Changed {
		u.Fields[Field_UpdatedBy] = u.Actor
	}
	return Database.Model(target).Updates(u.Fields)
}

// POST parameter and table column must be the same

func (u *Updater) SetStr(key string) {
	val := u.Posts.GetStr(key)
	if key == `` {
		return
	}
	key = S.Trim(key)
	u.Fields[key] = S.XSS(val)
	u.Changed = true
}

func (u *Updater) SetPhone(key string) {
	val := u.Posts.GetStr(key)
	if key == `` {
		return
	}
	val = S.ValidatePhone(val)
	if val == `` {
		return
	}
	u.Fields[key] = val
	u.Changed = true
}

func (u *Updater) SetFloat(key string) {
	val := u.Posts.GetStr(key)
	if key == `` {
		return
	}
	key = S.Trim(key)
	u.Fields[key] = S.ToF(val)
	u.Changed = true
}

func (u *Updater) SetInt(key string) {
	val := u.Posts.GetStr(key)
	if key == `` {
		return
	}
	key = S.Trim(key)
	u.Fields[key] = S.ToI(val)
	u.Changed = true
}

func (u *Updater) SetRaw(key string, val interface{}) {
	u.Fields[key] = val
	u.Changed = true
}

func (u *Updater) SetNow(key string) {
	u.Fields[key] = &u.Now
	u.Changed = true
}

func (u *Updater) SetNull(key string) {
	u.Fields[key] = Null
	u.Changed = true
}

func (u *Updater) DeleteRestore(action string) {
	if S.StartsWith(action, `delete`) {
		u.Fields[Field_DeletedAt] = &u.Now
		u.Changed = true
	} else if S.StartsWith(action, `restore`) {
		u.Fields[Field_DeletedAt] = Null
		u.Changed = true
	}
}

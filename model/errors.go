package model

const (
	ERR_600_WRONG_USERNAME_OR_PASSWORD       = `600 wrong username or password`
	ERR_601_FAILED_TO_HASH_PASSWORD          = `601 failed to hash password`
	ERR_602_INVALID_EMAIL                    = `602 invalid email`
	ERR_603_EMAIL_ALREADY_USED               = `603 email already used`
	ERR_604_EMAIL_NOT_FOUND                  = `604 email not found on database`
	ERR_605_FAIL_SENDING_RESET_PASSWORD_LINK = `605 fail sending reset password link`
	ERR_606_RESET_LINK_INVALID_OR_EXPIRED    = `606 reset password link invalid or expired`
	ERR_607_INVALID_VERIFICATION_LINK        = `607 invalid verification link`
	ERR_608_INCORRECT_OLD_PASSWORD           = `608 incorrect old password`
	ERR_609_EMAIL_REQUIRED                   = `609 email required`
	ERR_610_MISSING_USER                     = `610 missing user: `
	ERR_611_RESET_PASS_LINK_SENT_EXPIRED_IN  = `611 reset password link already sent, minutes until expired: `
	ERR_612_FAILED_OAUTH                     = `612 failed oauth: `
	ERR_613_INVALID_CSRF_STATE               = `613 invalid csrf state: `
	ERR_614_NO_OAUTH_PROVIDER                = `614 no oauth provider fot host: `
	ERR_615_NO_EMAIL_FROM_OAUTH_PROVIDER     = `615 Cannot create account since no e-mail supplied from OAuth provider, please use another method to sign-in/sign-up`
	ERR_616_VERIFY_LINK_REQUESTED_TOO_SOON   = `616 verification link requested too many times, minutes to wait: `
)
